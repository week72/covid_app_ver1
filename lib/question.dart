import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var questions = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อนล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValues[0],
              title: Text(questions[0]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[0] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[1],
              title: Text(questions[1]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[1] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[2],
              title: Text(questions[2]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[2] = newvalue!;
                });
              }),
              CheckboxListTile(
              value: questionValues[3],
              title: Text(questions[3]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[3] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[4],
              title: Text(questions[4]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[4] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[5],
              title: Text(questions[5]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[5] = newvalue!;
                });
              }),
              CheckboxListTile(
              value: questionValues[6],
              title: Text(questions[6]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[6] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[7],
              title: Text(questions[7]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[7] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[8],
              title: Text(questions[8]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[8] = newvalue!;
                });
              }),
              CheckboxListTile(
              value: questionValues[9],
              title: Text(questions[9]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[9] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[10],
              title: Text(questions[10]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[10] = newvalue!;
                });
              }),

              ElevatedButton(
                  onPressed: () async {
                      await _saveQuestion();
                      Navigator.pop(context);

                  },
                  child: Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() { 
    super.initState();
    _loadQuestion();
  }


Future<void> _loadQuestion() async {
  var prefs = await SharedPreferences.getInstance();
  var strQuestionValue = prefs.getString('question_values')??
  '[false, false, false, false, false, false, false, false, false, false, false]';
  var arrStrQuestionValues = strQuestionValue.substring(1, strQuestionValue.length -1).split(',');
  setState(() {
    for(var i=0;i<arrStrQuestionValues.length;i++) {
      questionValues[i] = (arrStrQuestionValues[i].trim() == 'true');
    }
  });
}

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValues.toString());
  }
}
